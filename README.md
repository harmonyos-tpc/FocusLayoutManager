# FocusLayoutManager

RecyclerView-LayoutManager for horizontal/vertical scrolling of the focused item.

## Usage Instructions

### Library Dependencies
    FocusLayoutManager is dependent on recyclercomponent.

SHOWING DATA WITH STACKING HORIZONTAL (LEFT/RIGHT) DIRECTION
Create xml code as below :

	<RadioButton
            ohos:id="$+id:l"
            ohos:width="match_content"
            ohos:height="match_content"
            ohos:text="Stack Left"
            ohos:text_size="16fp"
            ohos:marked="true" />

	<RadioButton
            ohos:id="$+id:r"
            ohos:width="match_content"
            ohos:height="match_content"
            ohos:text="Stack right"
            ohos:text_size="16fp"/>


Create java code as below :

	focusLayoutManager.setFocusOrientation(FocusLayoutManager.FOCUS_LEFT);

	recyclerView.setLayoutConfig(new ComponentContainer.LayoutConfig(HarmonyConstant.getMaximumWindowMetrics(this).getWidth(),
                             HarmonyConstant.getMaximumWindowMetrics(this).getHeight() / 4));

	recyclerView.setAdapter(new Adapter(this, datas = getDatas( false)));

![Image](/image/screen_horizontal.png)

SHOWING DATA WITH STACKING VERTICAL (T0P/BOTTOM) DIRECTION
Create xml code as below :

	<RadioButton
            ohos:id="$+id:t"
            ohos:width="match_content"
            ohos:height="match_content"
            ohos:text="Stack Up"
            ohos:text_size="16fp"/>

	 <RadioButton
            ohos:id="$+id:b"
            ohos:width="match_content"
            ohos:height="match_content"
            ohos:text="Stack Down"
            ohos:text_size="16fp"/>


Create java code as below :

	focusLayoutManager.setFocusOrientation(FocusLayoutManager.FOCUS_TOP);

	recyclerView.setLayoutConfig(new ComponentContainer.LayoutConfig(HarmonyConstant.getMaximumWindowMetrics(this).getWidth()/4,
                             HarmonyConstant.getMaximumWindowMetrics(this).getHeight() / 2));

	recyclerView.setMarginsLeftAndRight(400, 500);

	recyclerView.setAdapter(new Adapter(this, datas = getDatas(true)));


<img src="./images/screen_vertical.png" width=250 ></img>

Setting normal and common view gap
Add below java api :

	focusLayoutManager.setLayerPadding(dp2px(this, count));

	focusLayoutManager.setNormalViewGap(dp2px(this, count));

<img src="./images/screen_margin.png" width=250 ></img>

Setting max layer count
Add below java api :

	focusLayoutManager.setMaxLayerCount(count);

<img src="./images/screen_maxlayercount.png" width=250 ></img>



## Installation Tutorial

Solution 1: local har package integration, Add the .har package to the lib folder.
    Add the following code to the gradle of the entry:

    dependencies {
        implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    }

Solution 2: Add following dependencies in your build.gradle:
    Gradle

    dependencies {
        implementation project(':focuslayoutmanager')
        implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    }