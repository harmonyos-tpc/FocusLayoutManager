/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package ccy.focuslayoutmanagerproject.slice;

import ccy.focuslayoutmanager.utils.LogUtil;
import ccy.focuslayoutmanagerproject.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Image;
import ohos.app.Context;

import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;

public class DetailAbilitySlice extends AbilitySlice {
    private static final String TAG = "DetailAbilitySlice";
    private static final String RESOURCE_ID = "resId";
    private static final String DETAILS = "details";

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_detail_act);

        IntentParams test= intent.getParam(DETAILS);
        int res = Integer.parseInt(test.getParam(RESOURCE_ID).toString());

        String path = getPathById(this, res);
        RawFileEntry assetManager = this.getResourceManager().getRawFileEntry(path);
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        options.formatHint = "image/png";
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();

        Resource asset = null;
        try {
            asset = assetManager.openRawFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ImageSource source = ImageSource.create(asset, options);
        PixelMap pixelMap = source.createPixelmap(decodingOptions);

        ((Image) findComponentById(ResourceTable.Id_img)).setPixelMap(pixelMap);

    }

    String getPathById(Context context, int viewPathId) {
        String path = "";
        if (context == null) {
            LogUtil.error(TAG, "getPathById -> get null context");
            return path;
        }
        ResourceManager manager = context.getResourceManager();
        if (manager == null) {
            LogUtil.error(TAG, "getPathById -> get null ResourceManager");
            return path;
        }
        try {
            path = manager.getMediaPath(viewPathId);
        } catch (IOException e) {
            LogUtil.error(TAG, "getPathById -> IOException");
        } catch (NotExistException e) {
            LogUtil.error(TAG, "getPathById -> NotExistException");
        } catch (WrongTypeException e) {
            LogUtil.error(TAG, "getPathById -> WrongTypeException");
        }
        return path;
    }
}
